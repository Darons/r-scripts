# R Scripts

Algorithms made using R language.

### Content
* Inventory models.
	* Classic EOQ model.
	* EOQ with limited storage space.
	* EOQ when discounts are allowed.
	* Probabilistic EOQ
* Normal table.
* Replacement models. 
	* Lineal
	* Non lineal
	* With taxes.
	* With no taxes.
	* With scheduled obsolescence.
	* Maintenance model.
