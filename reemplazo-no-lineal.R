#Reemplazo con funciones no lineales

nolineal = function(I, co, cm, k, n=-1, t=0)
{
	#Inversion, operacion y mantenimiento al primero año, ajuste, n para comparaciones, reventa si se desea

	noptimo = ( (I - t) / (k * (co + cm))) ^ (1 / (k + 1))
	ctp = (I - t) / noptimo + (co + cm) * noptimo ^ k

	print(c(paste("Período de reemplazo óptimo:"), round(noptimo,2)))
	print(c(paste("Costo mínimo de reemplazo:"), round(ctp,2)))

	if (n != -1)
	{
		ctp2 = (I - t) / n + (co + cm) * n ^ k
		print(c(paste("Costo mínimo de reemplazo al período dado:"), round(ctp2,2)))
	}
}

I=3000
co=1500
cm=200
k=0.25

nolineal(I,co,cm,k)
nolineal(I,co,cm,k,4)
nolineal(I,co,cm,k,t=1500)
nolineal(I,co,cm,k,4,1500)
